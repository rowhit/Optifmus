#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <QDialog>
#include <QTreeWidgetItem>
#include <QKeyEvent>

#include "opt_project.h"
#include "plotgraphs.h"
#include "filetree.h"

namespace Ui {
class experiment;
}

class experiment : public QDialog
{
    Q_OBJECT

public:
    explicit experiment(QWidget *parent = 0);
    ~experiment();
    void checkEnabled();
    void setModelExp(opt_project *p);
    opt_exp getExp();

protected:
    // Experiment info
    unsigned     fileStatus;
    bool         fileChanged;
    opt_model    mo;
    opt_exp      exp;
    opt_project *prj;

    // File tree
    fileTree ft;

    // Internal info
    QList<variable> vars;
    QList<double> first;
    QList<double> last;
    QModelIndex   indexTime;
    void setFile(QString file);
    void clearFields();
    bool foundVariable(QString name);

private slots:
    void on_cbTimeType_currentIndexChanged(const QString &arg1);
    void on_bOpen_clicked();
    void on_eFile_textChanged(const QString &arg1);
    void on_toolButton_clicked();
    void on_lineFilter_returnPressed();
    void on_cb_currentIndexChanged(const QString &);
    void deleteWidgets(QTreeWidgetItem *item);
    void on_tb_clicked();
    void on_tbClear_clicked();
    void keyPressEvent(QKeyEvent *e);
    void on_tbTimeFile_clicked();
    void on_buttonBox_accepted();
    void on_tbExpImport_clicked();
    void on_tbDocImport_clicked();
    void on_tbRefresh_clicked();
    void on_bPlot_clicked();

    void on_leInitial_textChanged(const QString &arg1);

    void on_leFinal_textChanged(const QString &arg1);

private:
    Ui::experiment *ui;
    QList<QWidget *> lw;
};

#endif // EXPERIMENT_H

