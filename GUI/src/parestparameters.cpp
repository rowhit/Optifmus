#include "parestparameters.h"
#include "ui_parestparameters.h"

#include "ui_conf.h"
#include "commonapp.h"

#include <QSpinBox>
#include <QPushButton>

const int COL_NAME    = 0;
const int COL_VALUE   = 1;
const int COL_DESC    = 2;
const int COL_UNIT    = 3;
const int COL_MIN     = 4;
const int COL_MAX     = 5;
const int COL_SC_TYPE = 6;
const int COL_SC_VAL  = 7;
const int COL_LAST    = 8;

const int WIDTH_NAME    = 100;
const int WIDTH_VALUE   = 75;
const int WIDTH_DESC    = 250;
const int WIDTH_UNIT    = 60;
const int WIDTH_MIN     = 75;
const int WIDTH_MAX     = 75;
const int WIDTH_SC_TYPE = 75;
const int WIDTH_SC_VAL  = 75;
const int WIDTH_LAST    = 10;

const unsigned W_VAL = 0;
const unsigned W_MIN = 1;
const unsigned W_MAX = 2;
const unsigned W_SCT = 3;
const unsigned W_SCV = 4;

const QString PRO_LE_VAL = "PRO_LE_VAL";
const QString PRO_LE_MIN = "PRO_LE_MIN";
const QString PRO_LE_MAX = "PRO_LE_MAX";
const QString PRO_PA_NAM = "PRO_PA_NAM";
const QString PRO_LE_SCT = "PRO_LE_SCT";
const QString PRO_LE_SCV = "PRO_LE_SCV";
const QString PRO_LE_ITE = "PRO_LE_ITE";


parestparameters::parestparameters(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parestparameters)
{
    ui->setupUi(this);
    readGeometry(this,pepWin);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);

    ui->lbFilter->setText(tFilter);
    ui->cbCaseSentitive->setText(tCaseSensitive);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_VALUE,WIDTH_VALUE);
    ui->tree->setColumnWidth(COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(COL_UNIT,WIDTH_UNIT);
    ui->tree->setColumnWidth(COL_MIN,WIDTH_MIN);
    ui->tree->setColumnWidth(COL_MAX,WIDTH_MAX);
    ui->tree->setColumnWidth(COL_SC_TYPE,WIDTH_SC_TYPE);
    ui->tree->setColumnWidth(COL_SC_VAL,WIDTH_SC_VAL);
    ui->tree->setColumnWidth(COL_LAST,WIDTH_LAST);

    QStringList headerLabels;

    headerLabels.push_back(tName);
    headerLabels.push_back(tValue);
    headerLabels.push_back(tDesc);
    headerLabels.push_back(tQuantityUnit);
    headerLabels.push_back(tMin);
    headerLabels.push_back(tMax);
    headerLabels.push_back(tScalingType);
    headerLabels.push_back(tScalingValue);
    headerLabels.push_back(sEmpty);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    readState(ui->tree->header(),treePep);
}

parestparameters::~parestparameters()
{
    writeGeometry(this,pepWin);
    writeState(ui->tree->header(),treePep);
    delete ui;
}

void parestparameters::on_buttonBox_accepted()
{
    accept();
}

void parestparameters::on_buttonBox_rejected()
{
    reject();
}

void parestparameters::on_lineFilter_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void parestparameters::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void parestparameters::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

bool setInputColor(QLineEdit *le)
{
    bool ok = le->hasAcceptableInput();

    QString style = ok ? "" :
                         "color: red;   font: normal; background-color: rgb(255, 200, 200)";
    le->setStyleSheet(style);
    return ok;
}

void parestparameters::checkEnabled()
{
    bool valid = true;
    int  i     = 0;

    while (valid && i < par.count())
    {
        valid = par[i].getValid();
        i++;
    }
    valid = valid && par.count()>0;
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(valid);
}

int findParam(QList<opt_parameter> par, QString name)
{
    bool found = false;
    int  i     = 0;

    while (!found && i<par.count())
    {
        found = par[i].getDescriptor() == name;
        if (!found) i++;
    }
    return found ? i : -1;
}

void parestparameters::on_lscv_textChanged(const QString &arg1)
{
   QLineEdit *le    = (QLineEdit *)sender();
   QString    name  = le->property(PRO_PA_NAM.toStdString().c_str()).toString();
   int        pos   = findParam(par,name);

   if (pos>=0) par[pos].setScalingValue(arg1.toDouble());
}

void parestparameters::on_le_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);

    QLineEdit *le   = (QLineEdit *)sender();
    QLineEdit *lval = (QLineEdit *)le->property(PRO_LE_VAL.toStdString().c_str()).value<void *>();
    QLineEdit *lmin = (QLineEdit *)le->property(PRO_LE_MIN.toStdString().c_str()).value<void *>();
    QLineEdit *lmax = (QLineEdit *)le->property(PRO_LE_MAX.toStdString().c_str()).value<void *>();

    double value = lval->text().toDouble();
    double min   = lmin->text().toDouble();
    double max   = lmax->text().toDouble();

    setValidator(lval,min,max);
    setValidator(lmin,-MAX_DOUBLE,value);
    setValidator(lmax,value,MAX_DOUBLE);

    bool    valid = setInputColor(lmin) && setInputColor(lmax) && setInputColor(lval);
    QString name  = lval->property(PRO_PA_NAM.toStdString().c_str()).toString();
    int     pos   = findParam(par,name);

    if (pos>=0)
    {
        par[pos].setInitialValue(value);
        par[pos].setLowerBound(min);
        par[pos].setUpperBound(max);
        par[pos].setValid(valid);
    }
    checkEnabled();
}

void setWidgetsLE(QLineEdit *le, QLineEdit *lval, QLineEdit *lmin, QLineEdit *lmax, QComboBox *cbsct, QLineEdit *lscv, QString name)
{
    le->setProperty(PRO_PA_NAM.toStdString().c_str(),name);
    le->setProperty(PRO_LE_VAL.toStdString().c_str(),qVariantFromValue((void *)lval));
    le->setProperty(PRO_LE_MIN.toStdString().c_str(),qVariantFromValue((void *)lmin));
    le->setProperty(PRO_LE_MAX.toStdString().c_str(),qVariantFromValue((void *)lmax));
    le->setProperty(PRO_LE_SCT.toStdString().c_str(),qVariantFromValue((void *)cbsct));
    le->setProperty(PRO_LE_SCV.toStdString().c_str(),qVariantFromValue((void *)lscv));
}

void setWidgetsCB(QComboBox *cb, QLineEdit *le, QTreeWidgetItem *item, QString name)
{
    cb->setProperty(PRO_PA_NAM.toStdString().c_str(),name);
    cb->setProperty(PRO_LE_SCV.toStdString().c_str(),qVariantFromValue((void *)le));
    cb->setProperty(PRO_LE_ITE.toStdString().c_str(),qVariantFromValue((void *)item));
}

void setEnabledLE(QLineEdit *le, QTreeWidgetItem *item)
{
    le->setEnabled(item->checkState(COL_NAME) == Qt::Checked);
}

void setEnabledCB(QComboBox *cb, QTreeWidgetItem *item)
{
    cb->setEnabled(item->checkState(COL_NAME) == Qt::Checked);
}

void setEnabledSCV(QLineEdit *lscv, QComboBox *cbsct, QTreeWidgetItem *item)
{
    lscv->setEnabled(item->checkState(COL_NAME) == Qt::Checked &&
                     cbsct->currentIndex() != scNone &&
                     cbsct->currentIndex() != scAuto);
}

void populateScalingCombo(QComboBox *cbsct)
{
    cbsct->clear();
    cbsct->insertItem(scNone,sctNone);
    cbsct->insertItem(scValue,sctValue);
    cbsct->insertItem(scLog,sctLog);
    cbsct->insertItem(scAuto,sctAuto);
}

void setItemTreeParEstParams(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int k, QList<QWidget *> &lw)
{
    Q_UNUSED(v);
    Q_UNUSED(vs);
    Q_UNUSED(lw);

    opt_model              m  = prj.getModel();
    QList<opt_parameter> par  = prj.getPrb().getParameters();
    QList<variable>      vars = prj.getType() == ptInputEst ? m.inputs : m.params;

    switch (vars[k].type)
    {
        case FMI_REAL:
        {
            // LineEdit - value, min and max
            QLineEdit *lval  = new QLineEdit(tree);
            QLineEdit *lmin  = new QLineEdit(tree);
            QLineEdit *lmax  = new QLineEdit(tree);
            QComboBox *cbsct = new QComboBox(tree);
            QLineEdit *lscv  = new QLineEdit(tree);

            // Item info
            item->setIcon(COL_NAME,QIcon(iReal));
            item->setText(COL_DESC,vars[k].desc);
            item->setText(COL_UNIT,formatedUnit(m,vars,k));
            item->setCheckState(COL_NAME,Qt::Unchecked);

            // Item widgets
            item->setData(W_VAL,Qt::UserRole,qVariantFromValue((void *)lval));
            item->setData(W_MIN,Qt::UserRole,qVariantFromValue((void *)lmin));
            item->setData(W_MAX,Qt::UserRole,qVariantFromValue((void *)lmax));
            item->setData(W_SCT,Qt::UserRole,qVariantFromValue((void *)cbsct));
            item->setData(W_SCV,Qt::UserRole,qVariantFromValue((void *)lscv));

            // Initial value, min, max, scaling type and scaling value
            double   value = vars[k].value.toDouble();
            double   min   = vars[k].min.toDouble();
            double   max   = vars[k].max.toDouble();
            unsigned sct;
            double   scv;

            // Changed parameter value
            int ix = m.p_name_new.indexOf(vars[k].name);
            if (ix>=0)
            {
                bool ok;

                m.p_value_new[ix].toDouble(&ok);
                if (ok) value = m.p_value_new[ix].toDouble();
            }

            // Change optimization parameter value
            int pos = findParam(par,vars[k].name);
            if (pos>=0)
            {
                item->setCheckState(COL_NAME,Qt::Checked);
                value = par[pos].getInitialValue();
                min   = par[pos].getLowerBound();
                max   = par[pos].getUpperBound();
            }

            min = min > value ? -MAX_DOUBLE : min;
            max = max < value ?  MAX_DOUBLE : max;
            sct = pos >= 0 ? par[pos].getScalingType()  : scNone;
            scv = pos >= 0 ? par[pos].getScalingValue() : 1;

            // Value
            setWidgetsLE(lval,lval,lmin,lmax,cbsct,lscv,vars[k].name);
            setEnabledLE(lval,item);
            setValidator(lval,min,max);
            lval->setText(QString::number(value));
            lval->connect(lval,SIGNAL(textChanged(const QString &)),tree->parent(),SLOT(on_le_textChanged(const QString &)));
            tree->setItemWidget(item,COL_VALUE,lval);

            // Min
            setWidgetsLE(lmin,lval,lmin,lmax,cbsct,lscv,vars[k].name);
            setEnabledLE(lmin,item);
            setValidator(lmin,-MAX_DOUBLE,value);
            lmin->setText(QString::number(min));
            lmin->connect(lmin,SIGNAL(textChanged(const QString &)),tree->parent(),SLOT(on_le_textChanged(const QString &)));
            tree->setItemWidget(item,COL_MIN,lmin);

            // Max
            setWidgetsLE(lmax,lval,lmin,lmax,cbsct,lscv,vars[k].name);
            setEnabledLE(lmax,item);
            setValidator(lmax,value,MAX_DOUBLE);
            lmax->setText(QString::number(max));
            lmax->connect(lmax,SIGNAL(textChanged(const QString &)),tree->parent(),SLOT(on_le_textChanged(const QString &)));
            tree->setItemWidget(item,COL_MAX,lmax);

            // Scaling type
            setEnabledCB(cbsct,item);
            setWidgetsCB(cbsct,lscv,item,vars[k].name);
            populateScalingCombo(cbsct);
            cbsct->setCurrentIndex(sct);
            cbsct->connect(cbsct,SIGNAL(currentIndexChanged(int)),tree->parent(),SLOT(on_cb_indexchanged(int)));
            tree->setItemWidget(item,COL_SC_TYPE,cbsct);

            // Scaling value
            setEnabledSCV(lscv,cbsct,item);
            setWidgetsLE(lscv,lval,lmin,lmax,cbsct,lscv,vars[k].name);
            setValidator(lscv,-MAX_DOUBLE,MAX_DOUBLE);
            lscv->setText(QString::number(scv));
            lscv->connect(lscv,SIGNAL(textChanged(const QString &)),tree->parent(),SLOT(on_lscv_textChanged(const QString &)));
            tree->setItemWidget(item,COL_SC_VAL,lscv);

            break;
        }
        case FMI_INTEGER: {break;}
        case FMI_STRING:  {break;}
        case FMI_BOOL:    {break;}
        default:          {break;}
    }
}

void parestparameters::on_cb_indexchanged(int index)
{
    QComboBox       *cb   = (QComboBox *)sender();
    QLineEdit       *le   = (QLineEdit *)cb->property(PRO_LE_SCV.toStdString().c_str()).value<void *>();
    QTreeWidgetItem *item = (QTreeWidgetItem *)cb->property(PRO_LE_ITE.toStdString().c_str()).value<void *>();
    QString          name = cb->property(PRO_PA_NAM.toStdString().c_str()).toString();

    setEnabledSCV(le,cb,item);

    int pos = findParam(par,name);
    if (pos>=0) par[pos].setScalingType(index);
}

void parestparameters::setParams(opt_project prj)
{
   ui->lTitle->setText(prj.getType() == ptInputEst ? tInputEstInputs : tParEstParams);
   setWindowTitle(prj.getType() == ptInputEst ? tInputEst : tParamConfig);

    m   = prj.getModel();
    par = prj.getPrb().getParameters();

    QList<variable> vars = prj.getType() == ptInputEst ? m.inputs : m.params;

    bool oldState = ui->tree->blockSignals(true);
    populateTreeGeneric(ui->tree,vars,prj,COL_NAME,sEmpty,setItemTreeParEstParams,lw);
    ui->tree->blockSignals(oldState);
    checkEnabled();
}

void parestparameters::on_tree_itemChanged(QTreeWidgetItem *item, int column)
{
    if (column == COL_NAME)
    {
        bool checked = !item->data(column,Qt::CheckStateRole).isValid() ||
                        item->checkState(COL_NAME) == Qt::Checked;

        QLineEdit *lval = (QLineEdit *)item->data(W_VAL,Qt::UserRole).value<void *>();
        QLineEdit *lmin = (QLineEdit *)item->data(W_MIN,Qt::UserRole).value<void *>();
        QLineEdit *lmax = (QLineEdit *)item->data(W_MAX,Qt::UserRole).value<void *>();
        QComboBox *csct = (QComboBox *)item->data(W_SCT,Qt::UserRole).value<void *>();
        QLineEdit *lscv = (QLineEdit *)item->data(W_SCV,Qt::UserRole).value<void *>();
        QString    name = lval->property(PRO_PA_NAM.toStdString().c_str()).toString();

        if (checked)
        {
            bool valid = setInputColor(lmin) && setInputColor(lmax) && setInputColor(lval);
            par.append(opt_parameter(name,lval->text().toDouble(),lmin->text().toDouble(),lmax->text().toDouble(),valid,csct->currentIndex(),lscv->text().toDouble()));
        }
        else
        {
            lval->setStyleSheet(sEmpty);
            lmin->setStyleSheet(sEmpty);
            lmax->setStyleSheet(sEmpty);
            int pos = findParam(par,name);
            if (pos>=0) par.removeAt(pos);
        }

        setEnabledLE(lval,item);
        setEnabledLE(lmin,item);
        setEnabledLE(lmax,item);
        setEnabledCB(csct,item);
        setEnabledSCV(lscv,csct,item);

        checkEnabled();
    }
}
