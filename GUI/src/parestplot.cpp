#include "parestplot.h"
#include "ui_parestplot.h"

#include <QtCharts/QScatterSeries>
#include <QtCharts/QValueAxis>
#include <QFileDialog>
#include <QGraphicsLayout>

#include "ui_conf.h"
#include <commonapp.h>

const qreal POINT_SIZE = 5;

parestplot::parestplot(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parestplot)
{
    ui->setupUi(this);
    readGeometry(this,p2DWin);

    setWindowTitle(t2DScatterGrp);

    ui->chartview->setFrameShape(QFrame::Shape::StyledPanel);
    ui->chartview->setRenderHint(QPainter::Antialiasing);
    ui->chartview->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->chartview->chart()->layout()->setContentsMargins(0, 0, 0, 0);
    ui->chartview->chart()->setBackgroundRoundness(0);

    const QIcon icClipboard = QIcon(iClipboard);
    const QIcon icFile      = QIcon(iFile);

    ui->actionExport_to_file->setIcon(icFile);
    ui->actionCopy_to_clipboard->setIcon(icClipboard);

    cmGraph.addAction(ui->actionExport_to_file);
    cmGraph.addAction(ui->actionCopy_to_clipboard);

    ui->bFile->setIcon(icFile);
    ui->bClipboard->setIcon(icClipboard);
}

parestplot::~parestplot()
{
    writeGeometry(this,p2DWin);
    delete ui;
}

void parestplot::plot2D(opt_result x, opt_result y)
{
    QChart         *chart = ui->chartview->chart();
    QScatterSeries *s     = new QScatterSeries();

    for(int i=0;i<x.getValues().count();i++)
        s->append(x.getValues()[i],y.getValues()[i]);

    s->setMarkerShape(QScatterSeries::MarkerShapeCircle);
    s->setBorderColor(s->color());
    s->setMarkerSize(POINT_SIZE);
    chart->addSeries(s);
    chart->createDefaultAxes();
    QValueAxis *ax = (QValueAxis *)chart->axes(Qt::Horizontal,s)[0];
    QValueAxis *ay = (QValueAxis *)chart->axes(Qt::Vertical,s)[0];
    ax->setTitleText(x.getName());
    ay->setTitleText(y.getName());
    chart->legend()->hide();
    chart->setDropShadowEnabled(false);
    ui->chartview->setFocus();
}

void parestplot::on_actionExport_to_file_triggered()
{
      QFileDialog dlg(this, mResultParEstSave, sEmpty, PDFFilter + ";;" + SVGFilter + ";;" + PNGFilter + ";;" + BMPFilter + ";;" + JPGFilter + ";;" + JPEGFilter);

      dlg.setAcceptMode(QFileDialog::AcceptSave);
      dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

      if (dlg.exec())
      {
        if (dlg.selectedNameFilter() == PDFFilter)
            chartToPDF(ui->chartview,dlg.selectedFiles()[0]);
        else if (dlg.selectedNameFilter() == SVGFilter)
            chartToSVG(ui->chartview,dlg.selectedFiles()[0]);
        else
            chartToImage(ui->chartview,false,dlg.selectedFiles()[0]);
      }
}

void parestplot::on_chartview_customContextMenuRequested(const QPoint &pos)
{
    cmGraph.exec(ui->chartview->viewport()->mapToGlobal(pos));
}

void parestplot::on_actionCopy_to_clipboard_triggered()
{
    chartToImage(ui->chartview);
}

void parestplot::on_bFile_clicked()
{
    ui->actionExport_to_file->trigger();
}

void parestplot::on_bClipboard_clicked()
{
    ui->actionCopy_to_clipboard->trigger();
}
