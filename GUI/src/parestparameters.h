#ifndef PARESTPARAMETERS_H
#define PARESTPARAMETERS_H

#include <QDialog>
#include <QKeyEvent>
#include <QTreeWidgetItem>

#include "opt_project.h"
#include "opt_problem.h"

namespace Ui {
class parestparameters;
}

class parestparameters : public QDialog
{
    Q_OBJECT

public:
    explicit parestparameters(QWidget *parent = 0);
    ~parestparameters();
    void setParams(opt_project prj);
    void checkEnabled();
    QList<opt_parameter> getPar(){return par;}

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_lineFilter_returnPressed();
    void on_tbClear_clicked();
    void keyPressEvent(QKeyEvent *e);
    void on_le_textChanged(const QString &arg1);
    void on_tree_itemChanged(QTreeWidgetItem *item, int column);
    void on_cb_indexchanged(int index);
    void on_lscv_textChanged(const QString &arg1);

private:
    Ui::parestparameters *ui;
    opt_model m;
    QList<opt_parameter> par;
    QList<QWidget *> lw;
};

#endif // PARESTPARAMETERS_H
