#ifndef PARESTRESULTS_H
#define PARESTRESULTS_H

#include <QDialog>
#include <QKeyEvent>
#include <QMenu>
#include <QTableWidget>


#include "opt_problem.h"

namespace Ui {
class parestresults;
}

class parestresults : public QDialog
{
    Q_OBJECT

public:
    explicit parestresults(QWidget *parent = 0);
    ~parestresults();
    void setData(QList<opt_result> p, QList<opt_result> o, QList<opt_result> c);

private:
    Ui::parestresults *ui;
    int numPar;
    int numObj;
    QList<opt_result> res;
    QMenu cmTable;
    void setEnabled();
    void tableToFile(QTableWidget *table, int top, int left, int rowCount, int colCount, bool header = false);

private slots:
    void keyPressEvent(QKeyEvent *e);
    void on_buttonBox_accepted();
    void on_cb2dx_currentIndexChanged(int index);
    void on_cb2dy_activated(const QString &arg1);
    void on_cb3dx_activated(const QString &arg1);
    void on_cb3dy_activated(const QString &arg1);
    void on_cb3dz_activated(const QString &arg1);
    void on_bPlot2d_clicked();
    void on_bPlot3d_clicked();
    void on_bClipboard_clicked();
    void on_table_customContextMenuRequested(const QPoint &pos);
    void on_actionCopy_selected_range_triggered();
    void on_chbHeader_clicked();
    void on_bFile_clicked();
    void on_actionExport_to_file_triggered();
};

#endif // PARESTRESULTS_H
