#--------------------------------------------------#
#                                                  #
# Project created by QtCreator 2016-11-16T14:17:39 #
#                                                  #
#--------------------------------------------------#

# Use Sundials for FMI++
DEFINES += USE_SUNDIALS

win32-g++: DEFINES += WIN32 MINGW

# Define release or debug build
CONFIG(release, debug|release) {
    DEFINES += RELEASE
} else {
    DEFINES += DEBUG
}

#------------------------------------------------------------#
# Precompiled header                                         #
#------------------------------------------------------------#
#DEFINES += USING_PCH
#PRECOMPILED_HEADER = stable.h

#------------------------------------------------------------#
# Version is read from file "version"                        #
#------------------------------------------------------------#
FILE_VERSION  = "$$cat(version)"
SPLIT_VERSION = $$split(FILE_VERSION,".")
.
APP_VERSION_MAYOR = $$member(SPLIT_VERSION,0)
APP_VERSION_MINOR = $$member(SPLIT_VERSION,1)
APP_VERSION_BUILD = $$member(SPLIT_VERSION,2)

DEFINES += VERSION_MAJOR=$${APP_VERSION_MAYOR}
DEFINES += VERSION_MINOR=$${APP_VERSION_MINOR}
DEFINES += VERSION_BUILD=$${APP_VERSION_BUILD}

VERSION = $$sprintf("%1.%2.%3",$${APP_VERSION_MAYOR},$${APP_VERSION_MINOR},$${APP_VERSION_BUILD})

#------------------------------------------------------------#
# Libraries' paths                                           #
#------------------------------------------------------------#

COMMON_SRC    = $${PWD}/FMI_common/common
COMMONGUI_SRC = $${PWD}/FMI_common/common_gui

FMIPP_SRC     = $${PWD}/../../../../ThirdParty/src/fmipp-code
DAKOTA_SRC    = $${PWD}/../../../../ThirdParty/src/dakota-6.5.0.src

unix:      QUAZIP_SRC = $${PWD}/../../../../ThirdParty/src/quazip-0.7.3/quazip
win32-g++: QUAZIP_SRC = $${PWD}/../../../../ThirdParty/src/quazip-0.7.2/quazip

FMIPP_LIB  = $${PWD}/../../../../ThirdParty/build/fmipp-build
DAKOTA_LIB = $${PWD}/../../../../ThirdParty/build/dakota-build
QUAZIP_LIB = $${PWD}/../../../../ThirdParty/build/quazip-build
MPI_SRC    = "/usr/lib64/mpi/gcc/openmpi/include/"
MPI_LIB    = "/usr/lib64/mpi/gcc/openmpi/lib64/"

win32-g++{
SUNDIALS_SRC  = $${PWD}/../../../../ThirdParty/src/sundials-2.7.0
SUNDIALS_LIB  = $${PWD}/../../../../ThirdParty/build/sundials-build
MSYS2_LIB     = "C:\msys64\mingw32\lib"
}
#------------------------------------------------------------#

QT += core widgets printsupport charts datavisualization gui svg

#greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

TARGET = Optifmus
TEMPLATE = app

#QMAKE_CXXFLAGS += -O2
#QMAKE_CXXFLAGS += -fopenmp
#QMAKE_LFLAGS   += -fopenmp
#QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter -Wno-unused-local-typedefs -Wno-sign-compare \
#                         -Wno-unused-variable -Wno-unused-function


#----------------------------------------------------------------------------------------#
# Suppress warning from these sources                                                    #
#----------------------------------------------------------------------------------------#
QMAKE_CXXFLAGS += -isystem$$[FMIPP_SRC]/ \
                  -isystem$${DAKOTA_SRC}/src/ \
                  -isystem$${DAKOTA_SRC}/packages/external/teuchos/packages/teuchos/src/ \
                  -isystem$${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/ \
                  -isystem$${DAKOTA_SRC}/packages/pecos/src/ \
#----------------------------------------------------------------------------------------#

INCLUDEPATH += $${COMMON_SRC} \                                                  # Common
               $${COMMONGUI_SRC} \                                               # Common_gui
               $${MPI_SRC} \                                                     # MPI
               $${FMIPP_SRC}  \                                                  # FMI++
               $${QUAZIP_SRC} \                                                  # Quazip
               $${DAKOTA_SRC}/src/ \                                             # Dakota
               $${DAKOTA_SRC}/packages/external/teuchos/packages/teuchos/src/  \ # Dakota
               $${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/  \ # Dakota
               $${DAKOTA_SRC}/packages/pecos/src/                                # Dakota

win32-g++{
INCLUDEPATH += $${SUNDIALS_SRC} \
}

SOURCES +=  main.cpp\
            qfmioptmain.cpp \
            parameters.cpp \
            experiment.cpp \
            filetree.cpp \
            modelstructure.cpp \
            parestalgorithm.cpp \
            parestparameters.cpp \
            parestobjective.cpp \
            fmuinfo.cpp \
            parestresults.cpp \
            parestplot.cpp \
            parestplot3d.cpp \
            parestconstraints.cpp \
            about.cpp \
            options.cpp \
            $${COMMON_SRC}/formats/read_csv.c \
            $${COMMON_SRC}/formats/read_matlab4.c \
            $${COMMON_SRC}/formats/libcsv.c \
            $${COMMON_SRC}/formats/write_matlab4.c \
            $${COMMON_SRC}/dakota/dakota_methods.cpp \
            $${COMMON_SRC}/dakota/fmuinterface.cpp \
            $${COMMON_SRC}/opt_problem.cpp \
            $${COMMON_SRC}/opt_project.cpp \
            $${COMMON_SRC}/common.cpp \
            $${COMMON_SRC}/simulation.cpp \
            $${COMMON_SRC}/parameterestimation.cpp \
            $${COMMON_SRC}/parest.cpp \
            $${COMMON_SRC}/inputestimation.cpp \
            $${COMMONGUI_SRC}/commonapp.cpp \
            $${COMMONGUI_SRC}/integrator.cpp \
            $${COMMONGUI_SRC}/plotgraphs.cpp \
            $${COMMONGUI_SRC}/seriesconfig.cpp \
            $${COMMONGUI_SRC}/penconfig.cpp \
            $${COMMONGUI_SRC}/brushconf.cpp \
            $${COMMONGUI_SRC}/graphconfig.cpp \
            $${COMMONGUI_SRC}/outputs.cpp

HEADERS  += confapp.h \
            qfmioptmain.h \
            ui_conf.h \
            parameters.h \
            experiment.h \
            filetree.h \
            modelstructure.h \
            parestalgorithm.h \
            parestparameters.h \
            parestobjective.h \
            fmuinfo.h \
            parestresults.h \
            parestplot.h \
            parestplot3d.h \
            parestconstraints.h \
            about.h \
            stable.h \
            options.h \
            $${COMMON_SRC}/formats/read_csv.h \
            $${COMMON_SRC}/formats/read_matlab4.h \
            $${COMMON_SRC}/formats/libcsv.h \
            $${COMMON_SRC}/formats/write_matlab4.h \
            $${COMMON_SRC}/dakota/dakota_methods.h \
            $${COMMON_SRC}/dakota/fmuinterface.h \
            $${COMMON_SRC}/warnings_off.h \
            $${COMMON_SRC}/warnings_on.h \
            $${COMMON_SRC}/opt_problem.h \
            $${COMMON_SRC}/opt_project.h \
            $${COMMON_SRC}/common.h \
            $${COMMON_SRC}/simulation.h \
            $${COMMON_SRC}/parameterestimation.h \
            $${COMMON_SRC}/parest.h \
            $${COMMON_SRC}/inputestimation.h \
            $${COMMONGUI_SRC}/commonapp.h \
            $${COMMONGUI_SRC}/integrator.h \
            $${COMMONGUI_SRC}/plotgraphs.h \
            $${COMMONGUI_SRC}/seriesconfig.h \
            $${COMMONGUI_SRC}/penconfig.h \
            $${COMMONGUI_SRC}/brushconf.h \
            $${COMMONGUI_SRC}/graphconfig.h \
            $${COMMONGUI_SRC}/outputs.h

FORMS    += qfmioptmain.ui \
            parameters.ui \
            experiment.ui \
            filetree.ui \
            modelstructure.ui \
            parestalgorithm.ui \
            parestparameters.ui \
            parestobjective.ui \
            fmuinfo.ui \
            parestresults.ui \
            parestplot.ui \
            parestplot3d.ui \
            parestconstraints.ui \
            about.ui \
            options.ui \
            $${COMMONGUI_SRC}/integrator.ui \
            $${COMMONGUI_SRC}/plotgraphs.ui \
            $${COMMONGUI_SRC}/seriesconfig.ui \
            $${COMMONGUI_SRC}/penconfig.ui \
            $${COMMONGUI_SRC}/brushconf.ui \
            $${COMMONGUI_SRC}/graphconfig.ui \
            $${COMMONGUI_SRC}/outputs.ui

# OpenMP support
# --------------
QMAKE_CXXFLAGS += -fopenmp
LIBS           += -fopenmp

# MPI Settings
# ------------
#QMAKE_CFLAGS           += $$system(mpicc --showme:compile)
#QMAKE_LFLAGS           += $$system(mpicxx --showme:link)
#QMAKE_CXXFLAGS         += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK
#QMAKE_CXXFLAGS_RELEASE += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK

# Binary for Linux
# ----------------
QMAKE_RPATHDIR += $${FMIPP_LIB}/import/ \
                  $${FMIPP_LIB}/export/ \
                  $${QUAZIP_LIB} \
                  $${DAKOTA_LIB}/src/ \
                  $${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/ \
                  $${DAKOTA_LIB}/packages/pecos/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/FrontEnd/Core/ \
                  $${DAKOTA_LIB}/packages/external/NOMAD/src/ \
                  $${DAKOTA_LIB}/packages/external/OPTPP/lib/ \
                  $${DAKOTA_LIB}/packages/external/hopspack/src/ \
                  $${MPI_LIB} \

win32-g++{
LIBS     += -L$${DAKOTA_LIB}/src/ -ldakota_src \                                          # Dakota
            -L$${DAKOTA_LIB}/src/ -ldakota_src_fortran \                                  # Dakota
            -L$${DAKOTA_LIB}/src/ -ldakota_dll_api \                                      # Dakota
            -L$${DAKOTA_LIB}/packages/external/nidr/ -lnidr \                             # Dakota
            -L$${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/ -lteuchos \  # Dakota
            -L$${DAKOTA_LIB}/packages/pecos/ -lpecos \                                    # Dakota
            -L$${DAKOTA_LIB}/packages/pecos/src -lpecos_src \                             # Dakota
            -L$${DAKOTA_LIB}/packages/external/LHS/lib/ -llhs \                           # Dakota
            -L$${DAKOTA_LIB}/packages/external/LHS/lib/ -llhs_mods \                      # Dakota
            -L$${DAKOTA_LIB}/packages/external/LHS/lib/ -llhs_mod \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/dfftpack/ -ldfftpack \                     # Dakota
            -L$${DAKOTA_LIB}/packages/external/VPISparseGrid/src/ -lsparsegrid \          # Dakota
            -L$${DAKOTA_LIB}/packages/surfpack/src -lsurfpack \                           # Dakota
            -L$${DAKOTA_LIB}/packages/surfpack/src -lsurfpack_fortran \                   # Dakota
            -L$${DAKOTA_LIB}/packages/surfpack/src -lsurfpack_c_interface \               # Dakota
            -L$${DAKOTA_LIB}/packages/external/approxnn/src/ -lapproxnn \                 # Dakota
            -L$${DAKOTA_LIB}/packages/external/DDACE/src/ -lddace \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/dream/ -ldream \                           # Dakota
            -L$${DAKOTA_LIB}/packages/external/FSUDace/ -lfsudace \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/hopspack/src/ -lhopspack \                 # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/FrontEnd/Core/ -ljega_fe \            # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/MOGA/ -lmoga \                        # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/SOGA/ -lsoga \                        # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/src/ -ljega \                         # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/eddy/utilities/ -leutils \            # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/Utilities/ -lutilities \              # Dakota
            -L$${DAKOTA_LIB}/packages/external/NCSUOpt/ -lncsuopt \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/NL2SOL/ -lcport \                          # Dakota
            -L$${DAKOTA_LIB}/packages/external/NOMAD/src/ -lnomad \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/OPTPP/lib/ -loptpp \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/PSUADE/ -lpsuade \                         # Dakota
            -L$${DAKOTA_LIB}/packages/external/ampl/ -lamplsolver \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/CONMIN/src/ -lconmin \                     # Dakota
            -L$${FMIPP_LIB}/ -lfmippim \                                                  # FMI++
            -L$${FMIPP_LIB}/ -lfmippex \                                                  # FMI++
            -L$${QUAZIP_LIB}/release/ -lquazip \                                          # Quazip
            -L$${SUNDIALS_LIB}/src/cvode/ -lsundials_cvode \                              # Sundials
            -L$${SUNDIALS_LIB}/src/nvec_ser/ -lsundials_nvecserial \                      # Sundials
            -L$${MSYS2_LIB} \                                                             # Boost
            -lboost_system-mt \                                                           # Boost
            -lboost_serialization-mt \                                                    # Boost
            -lboost_filesystem-mt \                                                       # Boost
            -lboost_regex-mt \                                                            # Boost
            -lboost_program_options-mt \                                                  # Boost
            -lblas \                                                                      # Blas
            -llapack \                                                                    # Lapack
            -lgfortran \                                                                  # Gfortran
}

unix{
LIBS     += -L$${FMIPP_LIB} -lfmippim \                                                   # FMI++
            -L$${FMIPP_LIB} -lfmippex \                                                   # FMI++
            -L$${QUAZIP_LIB} -lquazip \                                                   # Quazip
            -L$${DAKOTA_LIB}/src/ -ldakota_src \                                          # Dakota
            -L$${DAKOTA_LIB}/src/ -ldakota_src_fortran \                                  # Dakota
            -L$${DAKOTA_LIB}/src/ -ldakota_dll_api \                                      # Dakota
            -L$${DAKOTA_LIB}/packages/external/nidr/ -lnidr \                             # Dakota
            -L$${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/ -lteuchos \  # Dakota
            -L$${DAKOTA_LIB}/packages/pecos/ -lpecos \                                    # Dakota
            -L$${DAKOTA_LIB}/packages/pecos/src -lpecos_src \                             # Dakota
            -L$${DAKOTA_LIB}/packages/external/LHS/lib/ -llhs \                           # Dakota
            -L$${DAKOTA_LIB}/packages/external/LHS/lib/ -llhs_mods \                      # Dakota
            -L$${DAKOTA_LIB}/packages/external/LHS/lib/ -llhs_mod \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/dfftpack/ -ldfftpack \                     # Dakota
            -L$${DAKOTA_LIB}/packages/external/VPISparseGrid/src/ -lsparsegrid \          # Dakota
            -L$${DAKOTA_LIB}/packages/surfpack/src -lsurfpack \                           # Dakota
            -L$${DAKOTA_LIB}/packages/surfpack/src -lsurfpack_fortran \                   # Dakota
            -L$${DAKOTA_LIB}/packages/surfpack/src -lsurfpack_c_interface \               # Dakota
            -L$${DAKOTA_LIB}/packages/external/approxnn/src/ -lapproxnn \                 # Dakota
            -L$${DAKOTA_LIB}/packages/external/DDACE/src/ -lddace \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/dream/ -ldream \                           # Dakota
            -L$${DAKOTA_LIB}/packages/external/FSUDace/ -lfsudace \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/hopspack/src/ -lhopspack \                 # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/FrontEnd/Core/ -ljega_fe \            # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/MOGA/ -lmoga \                        # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/SOGA/ -lsoga \                        # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/src/ -ljega \                         # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/eddy/utilities/ -leutils \            # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/Utilities/ -lutilities \              # Dakota
            -L$${DAKOTA_LIB}/packages/external/NCSUOpt/ -lncsuopt \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/NL2SOL/ -lcport \                          # Dakota
            -L$${DAKOTA_LIB}/packages/external/NOMAD/src/ -lnomad \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/OPTPP/lib/ -loptpp \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/PSUADE/ -lpsuade \                         # Dakota
            -L$${DAKOTA_LIB}/packages/external/ampl/ -lamplsolver \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/CONMIN/src/ -lconmin \                     # Dakota
            -L$${DAKOTA_LIB}/packages/external/acro/packages/lib/ -lcolin \               # Dakota
            -L$${DAKOTA_LIB}/packages/external/acro/packages/lib/ -linterfaces \          # Dakota
            -L$${DAKOTA_LIB}/packages/external/acro/packages/lib/ -lscolib \              # Dakota
            -L$${DAKOTA_LIB}/packages/external/acro/packages/lib/ -lpebbl \               # Dakota
            -L$${DAKOTA_LIB}/packages/external/acro/packages/lib/ -lutilib \              # Dakota
            -L$${DAKOTA_LIB}/packages/external/acro/tpl/tinyxml/ -ltinyxml \              # Dakota
            -L$${DAKOTA_LIB}/packages/external/acro/tpl/3po/ -l3po \                      # Dakota
            -L$${MPI_LIB} -lmpi -lmpi_cxx \                                               # MPI
            -ldl \                                                                        # libdl
            -lboost_system-mt \                                                           # Boost
            -lboost_serialization-mt \                                                    # Boost
            -lboost_filesystem-mt \                                                       # Boost
            -lboost_regex-mt \                                                            # Boost
            -lboost_program_options-mt \                                                  # Boost
            -lblas \                                                                      # Blas
            -llapack \                                                                    # Lapack
            -lgfortran \                                                                  # Gfortran
            -lsundials_cvode \                                                            # Sundials
            -lsundials_nvecserial \                                                       # Sundials
}

RESOURCES += icons.qrc
