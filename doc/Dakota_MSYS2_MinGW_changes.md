## File: boost\serialization\array.hpp

*serialize* function changed name by *make_array* for Boost version 1.63.

## File: <source_dakota>\packages\external\JEGA\src\Evaluators\ExternalEvaluator.cpp

Included the following code:

```C++
// MINGW32: strerror function is in string.h
#ifdef __MINGW32__
#include <string.h>
#endif
```

## File: <source_dakota>\packages\external\NOMAD\src\EvalPoint.hpp

Included and commented the following:

```C++
//#ifdef WINDOWS
#if defined(WINDOWS) && !defined(__MINGW32__) // It is not necessesary to define it for MinGW32
#ifndef isnan
#define isnan  // Missing definition
inline bool isnan ( double x ) { return x != x; }
#endif
#endif
```

## File: <source_dakota>\packages\external\NOMAD\src\EvalPoint.cpp

Included and commented the following:

```C++
//#ifdef WINDOWS
#if defined(WINDOWS) && !defined(__MINGW32__) // Not valid case for MinGW32        
	if ( isnan ( _bb_outputs[i].value() ) )
		return true;
#else
	if ( std::isnan ( _bb_outputs[i].value() ) )           
		return true;
#endif
```

## File: <source_dakota>\packages\external\OPTPP\include\globals.h

Included and commented the following:

```C++
//#if defined(_WIN32) || defined(_WIN64)
#if defined(_WIN32) || defined(_WIN64) 
#ifndef __MINGW32__
// Microsoft's starts with an underscore, which is their convention 
// for non-standard language extensions
// Leveraging boost::math copysign might be an alternative worth considering
#define copysign _copysign
#endif
#endif
```

## File: <source_dakota>\packages\external\hopspack\src\src-main\HOPSPACK_Hopspack.cpp

Two times changed this:

```C++
    #if defined(HAVE_REALTIME_CLOCK)
        //#if defined(_WIN32) 
		#if defined(_WIN32) && !defined(__MINGW32__)
```
		
## File: <source_dakota>\packages\external\hopspack\src\src-main\HOPSPACK_SystemTimer.cpp

Changed all:

```C++
//#if defined(_WIN32)
#if defined(_WIN32) && !defined(__MINGW32__)
```

This must be checked. The following *define* was included before *#include<time.h>*.

```C++
#define _POSIX_C_SOURCE 1
```

## File: <source_dakota>\src\dakota_dll_api.cpp


Define BUILDING_DAKOTA_DLL before including *dakota_dll_api.h*.

```C++
//#include "dakota_dll_api.h"
#include <string>

#if defined(_WIN32) || defined(_MSC_VER) || defined(__MINGW32__)
#define BUILDING_DAKOTA_DLL
#endif

#include "dakota_dll_api.h"
```

## File: <source_dakota>\packages\external\ampl\fpinitmt.c
## File: <source_dakota>\external\acro\tpl\ampl\fpinitmt.c

Comment the following lines:

```C++
// Not needed now in MinGW32? -
//#ifdef __MINGW32__
//#define matherr _matherr
//#endif

// matherr_rettype
//matherr( struct _exception *e )
//{
//	switch(e->type) {
//	  case _DOMAIN:
//	  case _SING:
//		errno = set_errno(EDOM);
//		break;
//	  case _TLOSS:
//	  case _OVERFLOW:
//		errno = set_errno(ERANGE);
//	  }
//	return 0;
//	} 
```
