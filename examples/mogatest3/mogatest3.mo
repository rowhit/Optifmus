model mogatest3
  parameter Real x1 = 0 "Parameter x1";
  parameter Real x2 = 0 "Parameter x2";
  output Real f1 "Function f1";
  output Real f2 "Function f2";
  output Real h1 "Constraint h1";
  output Real h2 "Constraint h2";
equation
  f1 = (x1-2)^2 + (x2-1)^2 + 2;
  f2 = 9*x1 - (x2-1)^2;
  h1 = x1^2 + x2^2 - 225;
  h2 = x1 - 3*x2 + 10;
end mogatest3;
